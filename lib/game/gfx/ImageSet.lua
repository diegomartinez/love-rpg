
---
-- Image/tile set.
--
-- **Extends:** `klass`
--
-- @classmod game.gfx.ImageSet

local graphics = love and love.graphics

local klass = require "klass"
local ImageSet = klass:extend("game.gfx.ImageSet")

---
-- Source image.
--
-- @tfield table image
ImageSet.image = nil

---
-- Source image width.
--
-- @tfield number imagew
ImageSet.imagew = nil

---
-- Source image height.
--
-- @tfield number imageh
ImageSet.imageh = nil

---
-- Width of a single image.
--
-- @tfield number w
ImageSet.w = nil

---
-- Height of a single image.
--
-- @tfield number h
ImageSet.h = nil

---
-- Images per row.
--
-- @tfield number xcount
ImageSet.xcount = nil

---
-- Images per row.
--
-- @tfield number ycount
ImageSet.ycount = nil

---
-- Number of images.
--
-- @tfield number xcount
ImageSet.n = nil

---
-- Quads.
--
-- Mapping of `number` (frame index) to `love.graphics.Quad`.
--
-- @tfield table quads
ImageSet.quads = nil

---
-- Constructor.
--
-- @tparam string|love.graphics.Image image Image.
-- @tparam number xcount Images per row.
-- @tparam number ycount Images per column.
function ImageSet:init(image, xcount, ycount)
	if type(image) == "string" then
		image = graphics.newImage(image)
	end
	local siw, sih = image:getDimensions()
	local iw, ih = math.floor(siw/xcount), math.floor(sih/ycount)
	self.image = image
	self.xcount, self.ycount = xcount, ycount
	self.imagew, self.imageh = siw, sih
	self.w, self.h = iw, ih
	self.n = xcount * ycount
	self.quads = { }
end

local function getquad(self, index)
	local q = self.quads[index]
	if not q then
		local xcount = self.xcount
		local fx = self.w*((index-1)%xcount)
		local fy = self.h*math.floor((index-1)/xcount)
		local fw, fh = self.w, self.h
		q = graphics.newQuad(fx, fy, fw, fh, self.imagew, self.imageh)
		self.quads[index] = q
	end
	return q
end

---
-- Draw a sub-image by index.
--
-- @tparam number index Frame index.
-- @tparam ?number x X position. Default is 0.
-- @tparam ?number y Y position. Default is 0.
-- @tparam ?number rot Rotation angle in radians. Default is 0.
-- @tparam ?number px Pivot X position as percentage. Default is 0.
-- @tparam ?number py Pivot Y position as percentage. Default is 0.
function ImageSet:draw(index, x, y, rot, px, py)
	x, y, rot, px, py = x or 0, y or 0, rot or 0, px or 0, py or 0
	graphics.push()
	graphics.translate(x, y)
	graphics.rotate(rot)
	graphics.draw(self.image, getquad(self, index), -px*self.w, -py*self.h)
	graphics.pop()
end

---
-- Draw a sub-image by position.
--
-- @tparam number sx Source column.
-- @tparam number sy Source row.
-- @tparam ?number dx Destination X position. Default is 0.
-- @tparam ?number dy Destination Y position. Default is 0.
-- @tparam ?number rot Rotation angle in radians. Default is 0.
-- @tparam ?number px Pivot X position as percentage (0-1). Default is 0.
-- @tparam ?number py Pivot Y position as percentage (0-1). Default is 0.
function ImageSet:drawxy(sx, sy, dx, dy, rot, px, py)
	dx, dy, rot, px, py = dx or 0, dy or 0, rot or 0, px or 0, py or 0
	graphics.push()
	graphics.translate(dx, dy)
	graphics.rotate(rot)
	local q = graphics.newQuad(
			self.w*math.floor(sx), self.w*math.floor(sy),
			self.w, self.h, self.imagew, self.imageh)
	graphics.draw(self.image, q, -px*self.w, -py*self.h)
	graphics.pop()
end

return ImageSet
