
---
-- Graphics-related functions.
--
-- @module game.gfx
-- @see game.gfx.chargen
-- @see game.gfx.ImageSet

local graphics = love and love.graphics

local gfx = { }

gfx.chargen = require "game.gfx.chargen"
gfx.ImageSet = require "game.gfx.ImageSet"

---
-- Draw part of an image.
--
-- Source rectangle is in units of image size (e.g. 1 = width or height, 0.5 is
-- half that, and so on); destination rectangle is in current coordinate space.
--
-- @tparam love.graphics.Image image The source image.
-- @tparam ?number sx Source X.
-- @tparam ?number sy Source Y.
-- @tparam ?number sw Source width.
-- @tparam ?number sh Source height.
-- @tparam ?number dx Destination X.
-- @tparam ?number dy Destination Y.
-- @tparam ?number dw Destination width.
-- @tparam ?number dh Destination height.
-- @tparam ?number rot Rotation angle in radians.
-- @tparam ?number px Pivot X.
-- @tparam ?number py Pivot Y.
function gfx.drawimagepart(image, sx, sy, sw, sh, dx, dy, dw, dh, rot, px, py)
	local iw, ih = image:getDimensions()
	sx, sy, sw, sh = sx or 0, sy or 0, sw or 1, sh or 1
	dx, dy, px, py = dx or 0, dy or 0, px or 0, py or 0
	local q = graphics.newQuad(sx*iw, sy*iw, sw*iw, sh*ih, iw, ih)
	graphics.push()
	graphics.translate(dx, dy)
	if rot then
		graphics.rotate(rot)
	end
	graphics.translate(-px, -py)
	if dw or dh then
		graphics.scale((dw or sw)/sw, (dh or sh)/sh)
	end
	graphics.draw(image, q)
	graphics.pop()
end

return gfx
