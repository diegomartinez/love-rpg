
---
-- Character image generator.
--
-- @module game.gfx.chargen

local graphics = love.graphics

local chargen = { }

---
-- TODO: Doc.
--
-- @tfield table partdata
chargen.partdata = ({
	{ "feet",  5, 0, 2 },
	{ "hands", 6, 0, 2 },
	{ "lbody", 4, 1, 4 },
	{ "ubody", 3, 1, 6 },
	{ "neck",  7, 0, 2 },
	{ "hair",  1, 1, 2 },
	{ "head",  2, 0, 7 },
})

chargen.partdata.skin = { min=1, max=1 }
chargen.partdata.sclera = { min=1, max=2 }
chargen.partdata.iris = { min=1, max=1 }

for i, p in ipairs(chargen.partdata) do
	local key, index, min, max = unpack(p)
	p.key, p.index, p.min, p.max = key, index, min, max
	chargen.partdata[key] = p
end

local function drawpart(imageset, pid, psid, frame)
	imageset:draw((psid-1)*32 + (pid-1)*4 + 1 + math.floor(frame))
end

---
-- Draw a single frame.
--
-- @tparam game.entity.Character char Character data.
-- @tparam game.gfx.ImageSet imageset Image set.
-- @tparam number frame Frame index.
function chargen.drawsingle(char, imageset, frame)
	graphics.setColor(char.skin.color)
	drawpart(imageset, 1, 1, frame)
	graphics.setColor(char.sclera.color)
	drawpart(imageset, 1, char.sclera.index + 2, frame)
	graphics.setColor(char.iris.color)
	drawpart(imageset, 1, 2, frame)
	for i, info in ipairs(chargen.partdata) do
		local key, pid = unpack(info)
		local part = char[key]
		if part.index > 0 then
			graphics.setColor(part.color)
			drawpart(imageset, pid+1, part.index, frame)
		end
	end
end

---
-- Draw a full set of frames.
--
-- @tparam game.entity.Character char Character data.
-- @tparam game.gfx.ImageSet imageset Image set.
function chargen.drawset(char, imageset)
	for i = 0, 3 do
		graphics.push()
		graphics.translate(16*i, 0)
		chargen.drawchar(char, imageset, i)
		graphics.pop()
	end
end

---
-- Generate a full set of frames.
--
-- @tparam game.entity.Character char Character data.
-- @tparam game.gfx.ImageSet imageset Image set.
-- @treturn love.graphics.Image Image.
function chargen.generateset(char, imageset)
	local canvas = graphics.newCanvas(64, 64)
	graphics.setCanvas(canvas)
	chargen.drawset(char, imageset)
	graphics.setCanvas()
	return graphics.newImage(canvas:newImageData())
end

return chargen
