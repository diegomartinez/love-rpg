
---
-- Base class for entities.
--
-- **Extends:** `klass`
--
-- @classmod game.map.entity.Entity

local graphics = love and love.graphics
local utils = require "game.utils"

local klass = require "klass"
local Entity = klass:extend("game.map.entity.Entity")

---
-- X position.
--
-- @tfield number x
Entity.x = 0

---
-- Y position.
--
-- @tfield number y
Entity.y = 0

---
-- Entity animations.
--
-- @tfield game.gfx.ImageSet imageset
Entity.imageset = nil

---
-- Animation frames.
--
-- Array of sub-image indices (numbers).
--
-- @tfield table frames
Entity.frames = nil

---
-- Hit points.
--
-- @tfield number hp
Entity.hp = 0

---
-- Maximum hit points.
--
-- @tfield number hpmax
Entity.hpmax = 0

---
-- Animation frame index.
--
-- @tfield number frame
Entity.frame = nil

---
-- Time elapsed for current frame.
--
-- @tfield number frametime
Entity.frametime = nil

---
-- Constructor.
--
-- @tparam ?table fields Override fields.
function Entity:init(fields)
	if fields then
		for k, v in pairs(fields) do
			if type(v) == "table" then
				self[k] = utils.indexer(v)
			else
				self[k] = v
			end
		end
	end
end

---
-- Called to update the entity's logic.
--
-- @tparam number dtime Time since last call.
function Entity:update(dtime)
	self.frametime = self.frametime
	while true do
		local frame = self.frames[self.frame]
		if frame.time > self.frametime then
			break
		end
		self.frametime = self.frametime - frame.time
		self.frame = (self.frame % #self.frames) + 1
	end
end

---
-- Draw the entity.
--
function Entity:draw()
	graphics.push()
	graphics.translate(self.x, self.y)
	self:paint()
	graphics.pop()
end

---
-- Called to draw the entity.
--
function Entity:paint()
	if self.imageset and self.frames and self.frame then
		local frame = self.frames[self.frame]
		self.imageset:draw(frame.index)
	end
end

return Entity
