
---
-- TODO: Doc.
--
-- @module game.map.entity
-- @see game.map.entity.Character
-- @see game.map.entity.Entity

local entity = { }

entity.Character = require "game.map.entity.Character"
entity.Entity = require "game.map.entity.Entity"

return entity
