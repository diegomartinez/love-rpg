
---
-- Base class for characters.
--
-- **Extends:** `game.map.entity.Entity`
--
-- @classmod game.map.entity.Character

local Entity = require "game.map.entity.Entity"
local Character = Entity:extend("game.map.entity.Character")

---
-- Strength rating.
--
-- @tparam number str
Character.str = 0

---
-- Resistance rating.
--
-- @tparam number res
Character.res = 0

---
-- Intelligence rating.
--
-- @tparam number int
Character.int = 0

---
-- Willpower rating.
--
-- @tparam number wil
Character.wil = 0

---
-- Speed rating.
--
-- @tparam number spd
Character.spd = 0

Character.frames = { 0, 1, 2, 3 }

return Character
