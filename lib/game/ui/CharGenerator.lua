
---
-- TODO: Doc.
--
-- **Extends:** `simpleui.Box`
--
-- @classmod game.ui.CharGenerator

-- TODO: This is a mess.

local graphics = love.graphics
local Button = require "simpleui.Button"
local Label = require "simpleui.Label"
local Option = require "simpleui.Option"
local Slider = require "simpleui.Slider"
local Widget = require "simpleui.Widget"
local chargen = require "game.gfx.chargen"
local palette = require "game.data.palette"
local ColorPicker = require "game.ui.ColorPicker"

local Box = require "simpleui.Box"
local CharGenerator = Box:extend("game.ui.CharGenerator")

---
-- TODO: Doc.
--
-- @tfield table char
CharGenerator.char = nil

---
-- TODO: Doc.
--
-- @tfield game.gfx.ImageSet imageset
CharGenerator.imageset = nil

local attrs = { "str", "res", "int", "wil", "spd" }

local function randomcolor(min, max)
	return palette[math.random(min or 1, max or #palette)]
end

local function randomchar()
	local char = { }
	local count = 20
	char.str, char.res, char.int, char.wil, char.spd = 0, 0, 0, 0, 0
	char.points = 0
	while count > 0 do
		local a = attrs[math.random(1, #attrs)]
		if char[a] < 20 then
			char[a] = char[a] + 1
			count = count - 1
		end
	end
	char.skin = { index=1, color=randomcolor(25, 28) }
	char.sclera = { index=math.random(1, 2), color={ 255, 255, 255 } }
	char.iris = { index=1, color=randomcolor() }
	for _, p in ipairs(chargen.partdata) do
		char[p.key] = {
			index = math.random(p.min, p.max),
			color = randomcolor(),
		}
	end
	return char
end

---
-- TODO: Doc.
--
-- @tparam table params
function CharGenerator:init(params)
	Box.init(self, params)

	local curpart, curopt
	local partopts = { }
	local frameindex = 0
	local cpicker, islider, view, attrbox

	if not self.char then
		self.char = randomchar(self.char)
	end

	local function updateui()
		local p, pn = self.char[curpart], chargen.partdata[curpart]
		cpicker:setcolor(p.color)
		islider.increment = 1 / (pn.max - pn.min)
		islider.value = (p.index - pn.min) / (pn.max - pn.min)
		for i, k in ipairs({ "str", "res", "int", "wil", "spd", "points" }) do
			local v = self.char[k]
			attrbox[i][2].text  = tostring(v)
			attrbox[i][3].value = v/20
		end
	end

	local function selectpart(name)
		if curopt then
			curopt.value = false
		end
		curpart = name
		curopt = partopts[name]
		curopt.value = true
		updateui()
	end

	view = Widget {
		expand = true,
	}

	view.update = function(this, dtime)
		frameindex = (frameindex + dtime*4) % 4
	end

	view.paintbg = function(this)
		graphics.push()
		graphics.translate(this.w/2, this.h/2)
		graphics.scale(2)
		graphics.translate(-8, -8)
		chargen.drawsingle(self.char, self.imageset, frameindex)
		graphics.pop()
	end

	islider = Slider {
		minh=12,
		expand=true,
	}

	islider.valuechanged = function(this)
		local p, pn = self.char[curpart], chargen.partdata[curpart]
		p.index = math.floor(this.value*(pn.max-pn.min) + .5) + pn.min
	end

	cpicker = ColorPicker {
		minh = 48,
		columns = 16,
		palette = palette,
	}

	cpicker.colorchanged = function(this)
		self.char[curpart].color = this.color
	end

	local function makeattrslider(label, key)
		local l = Label {
			text = tostring(self.char[key]),
			texthalign = 1,
			minw = 16,
		}
		return Box {
			mode = "h",
			spacing = 4,
			Label {
				minw = 48,
				text = label,
			},
			l,
			Slider {
				value = self.char[key]/20,
				minw = 80,
				minh = 12,
				increment = 1/20,
				valuechanged = function(this, oldval)
					local val = math.floor(this.value*20+.5)
					local old = math.floor(oldval*20+.5)
					local diff = val - old
					if diff <= self.char.points then
						self.char.points = self.char.points - diff
						self.char[key] = val
					end
					updateui()
				end,
			},
		}
	end

	attrbox = Box {
		mode = "v",
		spacing = 4,
		makeattrslider("Strength",     "str"),
		makeattrslider("Resistance",   "res"),
		makeattrslider("Intelligence", "int"),
		makeattrslider("Willpower",    "wil"),
		makeattrslider("Speed",        "spd"),
		makeattrslider("Points left",  "points"),
	}

	attrbox[6].enabled = false

	local function generatechar()
		self:generated()
	end

	local function randomizechar()
		self.char = randomchar()
		updateui()
	end

	local function makepartoption(label, key)
		local o = Option {
			text = label,
			activated = function(this)
				selectpart(key)
				return Option.activated(this)
			end,
		}
		partopts[key] = o
		return o
	end

	self:addchild(Box {
		mode = "v",
		spacing = 4,
		padding = 4,
		expand = true,
		Box {
			mode = "h",
			expand = true,
			spacing = 4,
			attrbox,
			view,
		},
		Box {
			mode = "h",
			spacing = 4,
			Box {
				mode = "v",
				makepartoption("Skin",   "skin"),
				makepartoption("Sclera", "sclera"),
				makepartoption("Iris",   "iris"),
				makepartoption("Hair",   "hair"),
				makepartoption("Neck",   "neck"),
			},
			Box {
				mode = "v",
				makepartoption("Head",   "head"),
				makepartoption("UBody",  "ubody"),
				makepartoption("LBody",  "lbody"),
				makepartoption("Feet",   "feet"),
				makepartoption("Hands",  "hands"),
			},
			Box {
				mode = "v",
				spacing = 4,
				expand = true,
				Box {
					mode = "h",
					spacing = 4,
					Label { text="Type" },
					islider,
				},
				cpicker,
			},
		},
		Box {
			mode = "h",
			spacing = 4,
			Button {
				text = "Randomize",
				activated = randomizechar,
			},
			Button {
				text = "Generate",
				activated = generatechar,
			},
		},
	})

	selectpart("skin")

	self:layout()
end

---
-- TODO: Doc.
--
function CharGenerator:generated()
end

return CharGenerator
