
---
-- Color picker.
--
-- **Extends:** `simpleui.Widget`
--
-- @classmod game.ui.ColorPicker

local graphics = love.graphics

local Widget = require "simpleui.Widget"
local ColorPicker = Widget:extend("game.ui.ColorPicker")

---
-- Color.
--
-- @tfield table color
ColorPicker.color = nil

---
-- Color index.
--
-- @tfield number index
ColorPicker.index = nil

---
-- Palette.
--
-- @tfield table palette Array of 3 or 4 element tables.
ColorPicker.palette = ({ { 0, 0, 0, 255 } })

---
-- Columns.
--
-- @tfield number columns
ColorPicker.columns = nil

---
-- Columns.
--
-- @tfield number columns
ColorPicker.cellsize = 12

local function colortable(t)
	return { t[1] or 0, t[2] or 0, t[3] or 0, t[4] or 255 }
end

local function colorsequal(c1, c2)
	c1, c2 = colortable(c1), colortable(c2)
	return c1[1]==c2[1] and c1[2]==c2[2] and c1[3]==c2[3] and c1[4]==c2[4]
end

function ColorPicker:init(params)
	Widget.init(self, params)
	self:setcolor(self.color)
end

---
-- Set color.
--
-- @tparam number|table r Red component. Default is 0.
-- @tparam ?number g Green component. Default is 0.
-- @tparam ?number b Blue component. Default is 0.
-- @tparam ?number a Alpha component. Default is 255.
function ColorPicker:setcolor(r, g, b, a)
	local c = type(r) == "table" and colortable(r) or { r, g, b, a }
	for i, cc in ipairs(self.palette) do
		if colorsequal(c, cc) then
			local oi = self.colorindex
			self.index = i
			self.color = c
			if oi ~= i then
				self:colorchanged()
			end
			return true
		end
	end
end

---
-- Called when color changes.
--
function ColorPicker:colorchanged()
end

function ColorPicker:paintbg()
	local cols = self.columns
	if not cols then
		cols = math.floor(self.w/self.cellsize)
	end
	local function drawone(mode, index, off)
		local x = ((index-1)%cols)*self.cellsize
		local y = math.floor((index-1)/cols)*self.cellsize
		off = off or 0
		graphics.circle(mode,
				x+self.cellsize/2,
				y+self.cellsize/2,
				self.cellsize/2-1-off*2)
	end
	for i = 1, #self.palette do
		local c = self.palette[i]
		if c == nil then
			break
		end
		graphics.setColor(colortable(c))
		drawone("fill", i)
	end
	if self.index then
		graphics.setColor(0, 0, 0)
		drawone("line", self.index, -2)
		graphics.setColor(255, 255, 255)
		drawone("line", self.index, -1)
	end
end

function ColorPicker:mousepressed(x, y, b)
	local cols = self.columns
	if not cols then
		cols = math.floor(self.w/self.cellsize)
	end
	if b == self.LMB then
		x = math.floor(x/self.cellsize)
		y = math.floor(y/self.cellsize)
		local i = y*cols+x+1
		if x >= 0 and x < cols and i >= 0 and i <= #self.palette then
			self.index = i
			self.color = colortable(self.palette[i])
			self:colorchanged()
		end
	end
end

return ColorPicker
