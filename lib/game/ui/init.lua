
---
-- TODO: Doc.
--
-- @module game.ui
-- @see game.ui.CharGenerator
-- @see game.ui.ColorPicker

local ui = { }

ui.CharGenerator = require "game.ui.CharGenerator"
ui.ColorPicker = require "game.ui.ColorPicker"

return ui
