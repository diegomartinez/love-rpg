
---
-- Miscellaneous utilities.
--
-- @module game.utils

local utils = { }

---
-- TODO: Doc.
--
-- @tparam table parent
-- @treturn table indexer
function utils.indexer(parent)
	return setmetatable({ }, { __index = function(_, k)
		local v = parent[k]
		if type(v) == "table" then
			return utils.indexer(v)
		else
			return v
		end
	end })
end

return utils
