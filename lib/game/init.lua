
---
-- TODO: Doc.
--
-- @module game
-- @see game.gfx
-- @see game.map
-- @see game.ui
-- @see game.utils

local game = { }

game.gfx = require "game.gfx"
game.map = require "game.map"
game.ui = require "game.ui"
game.utils = require "game.utils"

return game
