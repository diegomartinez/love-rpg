
local graphics = love.graphics
local ImageSet = require "game.gfx.ImageSet"
local CharGenerator = require "game.ui.CharGenerator"

local function main()
	math.randomseed(os.time())
	graphics.setDefaultFilter("nearest", "nearest")
	graphics.setNewFont("data/gfx/KPixelFont.ttf", 8)

	local root = CharGenerator {
		imageset = ImageSet("data/gfx/chars.png", 32, 32),
	}

	function root:generated(image)
		image:getData():encode("png", "test.png")
		love.event.quit()
	end

	return root:run(2)
end

return main(arg)
