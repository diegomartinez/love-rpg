
--
-- Simple `luac` alternative.
--
-- The reason for this script is to compile the sources using the same
-- VM used by end product.
--

local argparse = require "argparse"

local parser = argparse("compile", "Compile lua files.")

parser:argument("input", "Set input file."):args("?")
parser:option("-o", "Set output file."):target("output")
		:default("luac.out"):argname("output")
parser:flag("-v", "Show version."):target("version")
parser:flag("-p", "Parse only."):target("parseonly")
parser:flag("-l", "Ignored."):target("list")
parser:flag("-s", "Ignored."):target("strip")

local function main(argv)
	table.remove(argv, 1)
	argv = parser:parse(argv)

	if argv.version then
		print(("%s/LÖVE %d.%d.%d (%s)"):format(_VERSION, love.getVersion()))
		return
	end

	if argv.input == "-" then
		argv.input = nil
	end

	local func = assert(loadfile(argv.input))

	if argv.parseonly then
		return
	end

	local outf = (argv.output == "-"
			and io.stdout
			or assert(io.open(argv.output, "w")))
	assert(outf:write(string.dump(func)))
	if argv.output ~= "-" then
		assert(outf:close())
	end
end

local exited, exitstatus
return xpcall(function()
	function os.exit(status) -- luacheck: ignore
		exited, exitstatus = true, status
		error("exit")
	end
	return love.event.quit(main(arg))
end, function(err)
	if not exited then
		io.stderr:write(err, "\n")
	end
	return love.event.quit(exitstatus)
end)
