
local fs = love.filesystem
fs.setRequirePath(
		"lib/?.lua;lib/?/init.lua;"
		.."lc/?.lc;lc/?/init.lc;?.lua;?.lc;"
		..fs.getRequirePath())

require "game.main"
