
.PHONY: default all love docs docsclean lint run clean distclean

default: all

APPNAME = rpg
APPVER = scm-0

ZIP ?= zip -u
ZIPFLAGS ?= -9

LDOC ?= ldoc
LDOCFLAGS ?=

LUACHECK ?= luacheck
LUACHECKFLAGS ?= --quiet --no-color

LOVE ?= love
LUAC ?= $(LOVE) util/compile

RM = rm -f
RMDIR = rm -fr
MKDIR = mkdir -p

all: love

MAINLOVE = $(APPNAME).love

MAINSRC = \
	lib/klass.lua \
	lib/gettext.lua \
	lib/minser.lua \
	lib/stringex.lua \
	\
	lib/simpleui/init.lua \
	lib/simpleui/Box.lua \
	lib/simpleui/Button.lua \
	lib/simpleui/Check.lua \
	lib/simpleui/Entry.lua \
	lib/simpleui/Image.lua \
	lib/simpleui/Label.lua \
	lib/simpleui/Option.lua \
	lib/simpleui/Slider.lua \
	lib/simpleui/Widget.lua \
	\
	lib/game/data/palette.lua \
	lib/game/map/entity/init.lua \
	lib/game/map/entity/Character.lua \
	lib/game/map/entity/Entity.lua \
	lib/game/map/init.lua \
	lib/game/gfx/init.lua \
	lib/game/gfx/chargen.lua \
	lib/game/gfx/ImageSet.lua \
	lib/game/ui/init.lua \
	lib/game/ui/CharGenerator.lua \
	lib/game/ui/ColorPicker.lua \
	lib/game/init.lua \
	lib/game/main.lua \
	lib/game/utils.lua

MAINOBJ = $(patsubst %.lua,%.lc,$(patsubst lib/%.lua,lc/%.lc,$(MAINSRC)))

MAINDATA = \
	data/gfx/chars.png \
	data/gfx/weapons.png \
	data/gfx/KPixelFont.ttf

love: $(MAINLOVE)

$(MAINLOVE): $(MAINOBJ) $(MAINDATA) conf.lua main.lua
	$(RM) $(MAINLOVE) && $(ZIP) $(ZIPFLAGS) $(MAINLOVE) \
			$(MAINOBJ) $(MAINDATA) conf.lua main.lua

lc/%.lc: lib/%.lua
	$(MKDIR) `dirname $@` && $(LUAC) -o $@ $<

docs:
	cd doc && $(RMDIR) api && $(LDOC) $(LDOCFLAGS) . -d api

docsclean:
	$(RMDIR) doc/api

lint:
	$(LUACHECK) $(LUACHECKFLAGS) .

run: $(MAINLOVE)
	$(LOVE) $(MAINLOVE)

clean:
	$(RM) $(MAINOBJ)

distclean: clean docsclean
	$(RM) $(MAINLOVE)
