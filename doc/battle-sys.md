
# Battle system

## Attributes

* Hit points:
  Determines life. Character dies at 0.
  Characters start with 20, and may raise it up to 100.
  Recovers at 1 p/s after 5s from last damage, while standing still.

* Skill points:
  Determines what skills can be used.
  Characters start with 20, and may raise it up to 100.
  Recovers at 2 p/s after 5s from last damage, while standing still.

* Strength rating:
  Determines physical offensive power.
  Maximum is 25.

* Resistance rating:
  Determines physical defensive power.
  Maximum is 25.

* Intelligence rating:
  Determines magical offensive power.
  Maximum is 25.

* Willpower rating:
  Determines magical defensive power.
  Maximum is 25.

## Formulas

	A = Attacker
	D = Defender

	pdmg = max(1, A.str - D.def/2)
	mdmg = max(1, A.int - D.wil/2)
	dmg = max(pdmg, mdmg)
