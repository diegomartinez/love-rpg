
# love-rpg

TODO: Summary.

## License

This is free software released under a MIT-like license.
See `LICENSE.md` for details.

## Requirements

* [LÖVE][love2d] version 0.10.2.

### Building from sources

* [klass][klass]
* [minser][minser]
* [simpleui][simpleui]
* [stringex][stringex]

[love2d]: http://love2d.org
[klass]: https://github.com/kaeza/lua-klass
[minser]: https://github.com/kaeza/lua-minser
[simpleui]: https://github.com/kaeza/love-simpleui
[stringex]: https://github.com/kaeza/lua-stringex
